
import java.io.*;
import java.util.*;

/**
 *
 * @author serdarozay
 */
public class IDTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<String[]> trans = GetTransactionFromFile();

        int index = getMaxEntropyAttributeIndex(getStatictic(trans));
        System.out.println(index);

    }

    private static int getMaxEntropyAttributeIndex(HashMap<Integer, HashMap<String, AttTypeStatistic>> statistics) {
        int maxEntropyIndex = 0;
        double maxEntropy = 0.0;
        for (Integer key : statistics.keySet()) {

            HashMap<String, AttTypeStatistic> types = statistics.get(key);
            double val = getEntropy(types);
            if (val > maxEntropy) {
                maxEntropyIndex = key;
                maxEntropy = val;
            } else if (val == maxEntropy) {
                System.out.println("ayni entropy ile kayit bulundu, ilk bulunan dogru kabul edildi.");
                //TODO: bilgi aktarilmali .. hangisi ? 
            }
        }
        return maxEntropyIndex;
    }

    private static double getEntropy(HashMap<String, AttTypeStatistic> types) {
        double entropy = 0.0;
        double entropyWithClass = 0.0;
        for (AttTypeStatistic type : types.values()) {
            double posibility = type.getCount() * 1.0 / type.getSampleSize();
            double truePosibility = type.getClasTrue() * 1.0 / type.getSampleSize();
            double falsePosibility = type.getClasTrue() * 1.0 / type.getSampleSize();
            entropy = entropy - (posibility * logOfBase(2, posibility));

            entropyWithClass = entropyWithClass - (truePosibility * logOfBase(2, truePosibility)) - (falsePosibility * logOfBase(2, falsePosibility));
        }

        return entropy - entropyWithClass;
    }

    private static double logOfBase(int base, double num) {
        return Math.log(num) / Math.log(base);
    }

    public static HashMap<Integer, HashMap<String, AttTypeStatistic>> getStatictic(List<String[]> trans) {
        if (trans == null || trans.size() == 0) {
            return null;
        }

        int attributeCount = trans.get(0).length - 1;
        int sampleCount = trans.size();
        String defaultTrue = trans.get(0)[attributeCount - 1];

        HashMap<Integer, HashMap<String, AttTypeStatistic>> statistic = new HashMap<Integer, HashMap<String, AttTypeStatistic>>();

        for (String[] attributes : trans) {
            if(attributes == null )
                continue;
            String className = attributes[attributeCount];
            System.out.println(className);
            for (int i = 0; i < attributeCount; i++) {
                if (statistic.get(i) == null) {
                    statistic.put(i, new HashMap<String, AttTypeStatistic>());
                }
                HashMap<String, AttTypeStatistic> types = statistic.get(i);
                if (types.containsKey(attributes[i])) {
                    types.get(attributes[i]).countValue(className);
                } else {
                    AttTypeStatistic st;
                    st = new AttTypeStatistic(attributes[i], defaultTrue,sampleCount);
                    types.put(attributes[i], st);
                    types.get(attributes[i]).countValue(className);
                }
            }
        }


        return statistic;

    }

    private static List<String[]> GetTransactionFromFile() {
        try {

            FileInputStream fstream = new FileInputStream("samples.txt");

            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;

            List<String[]> trans = new ArrayList<String[]>();
            while ((strLine = br.readLine()) != null) {

                SortedSet<String> items = new TreeSet<String>();
                String[] values = strLine.split(" ");
                trans.add(values);
            }

            in.close();
            return trans;
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            return null;
        }
    }
}
 class AttTypeStatistic {

    Integer[] counts = new Integer[]{0,0,0};
    String defTrue;
    String attName;
    int samplesize;

    public AttTypeStatistic(String name, String defaultTrue, int listSize) {
        attName = name;
        defTrue = defaultTrue;
        samplesize = listSize;
    }

    public void countValue(String className) {
        counts[0]++;
        if (className.equals(defTrue)) {
            counts[1]++;
        } else {
            counts[2]++;
        }
    }

    public int getCount() {
        return counts[0];
    }

    public int getClasTrue() {
        return counts[1];
    }

    public int getClasFalse() {
        return counts[1];
    }

    public int getSampleSize() {
        return samplesize;
    }
}
